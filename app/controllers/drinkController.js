const { default: mongoose } = require('mongoose');
const drinkModel = require('../models/drinkModel');

//Get all drinks
const getAllDrinks = (req, res) => {
    drinkModel.find()
        .then((data) => {
            if (data && data.length > 0) {
                return res.status(200).json({
                    status: `Get all drinks successfully`,
                    data
                })
            } else {
                return res.status(404).json({
                    status: `Not found any drinks`,
                    data
                })
            }
        })


}

//Get drink by Id
const getDrinkById = (req, res) => {
    //B1: thu thập dữ liệu:
    let drinkId = req.params.drinkId
    //B2: kiểm tra dữ liệu:
    if (!mongoose.Types.ObjectId.isValid(drinkId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Drink Id is invalid`
        })
    }
    //B3: thực thi model:
    drinkModel.findById(drinkId)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: `Get drink by Id successfully`,
                    data
                })
            } else {
                return res.status(404).json({
                    status: `Not found any drinks`,
                    data
                })
            }
        })
        .catch((error) => {
            return res.status(500).json({
                status: `Internal Server Error`,
                message: error.message
            })
        })

}

//Create new drink:
const createDrink = (req, res) => {
    console.log("Create new drink");
    //B1: thu thập dữ liệu:
    let { maNuocUong, tenNuocUong, donGia } = req.body
    //B2: kiểm tra dữ liệu:
    if (!maNuocUong) {
        return res.status(400).json({
            message: `maNuocUong is required`,
        })
    }
    if (!tenNuocUong) {
        return res.status(400).json({
            message: `tenNuocUong is required`,
        })
    }
    if (!Number.isInteger(donGia)) {
        return res.status(400).json({
            message: `donGia is invalid`,
        })
    }
    //B3: khoi tao model:
    let newDrink = new drinkModel({
        _id: new mongoose.Types.ObjectId(),
        maNuocUong: maNuocUong,
        tenNuocUong: tenNuocUong,
        donGia: donGia
    })
    drinkModel.create(newDrink)
        .then((data) => {
            return res.status(201).json({
                status: `Create new drink successfully`,
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: `Internal Server Error`,
                message: error.message
            })
        })
}

//Update drink:
const updateDrinkById = (req, res) => {
    //B1: thu thập dữ liệu:
    let drinkId = req.params.drinkId;
    let { maNuocUong, tenNuocUong, donGia } = req.body;

    //B2: kiểm tra dữ liệu:
    if (!mongoose.Types.ObjectId.isValid(drinkId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Drink Id invalid`
        })
    }
    if (!maNuocUong) {
        return res.status(400).json({
            message: `maNuocUong is required`,
        })
    }
    if (!tenNuocUong) {
        return res.status(400).json({
            message: `tenNuocUong is required`,
        })
    }
    if (!Number.isInteger(donGia)) {
        return res.status(400).json({
            message: `donGia is invalid`,
        })
    }
    //B3: thực thi model:
    let drinkUpdate = {
        maNuocUong,
        tenNuocUong,
        donGia
    }
    drinkModel.findByIdAndUpdate(drinkId, drinkUpdate)
        .then((data) => {
            if (data) {
                return res.status(200).json({
                    status: `Update drink by Id succesfully`,
                    data
                })
            } else {
                return res.status(404).json({
                    status: `Not found any drinks`,
                    data
                })
            }
        })
        .catch((error) => {
            return res.status(500).json({
                status: `Internal Server Error`,
                message: error.message
            })
        })

}


//delete drink:
const deleteDrink = (req, res) => {
    //B1: thu thập dữ liệu:
    let drinkId = req.params.drinkId;

    //B2: kiểm tra dữ liệu:
    if (!mongoose.Types.ObjectId.isValid(drinkId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Drink Id invalid`
        })
    }
    //B3: thực thi model:
    drinkModel.findByIdAndDelete(drinkId)
        .then((data) => {
            if (data) {
                return res.status(204).json({
                    status: `Delete drink by Id successfully`,
                    data
                })
            }else {
                return res.status(404).json({
                    status: `Not found any drinks`,
                    data
                })
            }
        })
        .catch((error) => {
            return res.status(500).json({
                status: `Internal Server Error`,
                message: error.message
            })
        })

}


module.exports = { getAllDrinks, getDrinkById, createDrink, updateDrinkById, deleteDrink }