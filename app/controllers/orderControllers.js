//import thư viện orderModel
const { default: mongoose } = require('mongoose');
const orderModel = require('../models/orderModel');

//create new order:
const createOrder = async (req, res) => {
    function makeid(length) {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        const charactersLength = characters.length;
        let counter = 0;
        while (counter < length) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
            counter += 1;
        }
        return result;
    }
    const orderCode = makeid(8);
    //B1: thu thập dữ liệu:
    const { pizzaSize, pizzaType, voucher, drink, status } = req.body;
    //B2: validate dữ liệu:
    if (!pizzaSize) {
        return res.status(400).json({
            status: `Bad request`,
            message: `pizzaSize is required!`
        })
    }
    if (!pizzaType) {
        return res.status(400).json({
            status: `Bad request`,
            message: `pizzaType is required!`
        })
    }
    if (!voucher) {
        return res.status(400).json({
            status: `Bad request`,
            message: `voucher is required!`
        })
    }
    if (!drink) {
        return res.status(400).json({
            status: `Bad request`,
            message: `drink is required!`
        })
    }
    if (!status) {
        return res.status(400).json({
            status: `Bad request`,
            message: `status is required!`
        })
    }
    // //B3: thực thi model
    let createOrderData = {
        _id: new mongoose.Types.ObjectId(),
        orderCode,
        pizzaSize,
        pizzaType,
        voucher,
        drink,
        status
    }
    try {
        const createdOrder = await orderModel.create(createOrderData);
        return res.status(201).json({
            status: `Create new order successfully`,
            data: createdOrder
        })
    }
    catch (error) {
        return res.status(500).json({
            status: `Internal Server Error`,
            message: error.message
        })
    }
}

//Get All Orders:
const getAllOrder = async (req, res) => {
    try {
        const orderList = await orderModel.find()
        if (orderList || orderList.length > 0) {
            return res.status(200).json({
                status: `Get all orders successfully!`,
                data: orderList
            })
        } else {
            return res.status(404).json({
                status: `Not found any orders`,
                data
            })
        }

    }
    catch (error) {
        return res.status(500).json({
            status: `Internal Server Error`,
            message: error.message
        })
    }
}

// //Get order by Id:
const getOrderById = async (req, res) => {
    //B1: thu thap du lieu
    const orderId = req.params.orderId;
    //B2: validate du lieu:
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `orderId is invalid!`
        })
    }
    try {
        const orderFoundById = await orderModel.findById(orderId);
        if (orderFoundById) {
            return res.status(200).json({
                status: `Get order by id ${orderId} successfully`,
                data: orderFoundById
            })
        } else {
            return res.status(404).json({
                status: `Do not found any orders`,
                data: orderFoundById
            })
        }
    }
    catch (error) {
        return res.status(500).json({
            status: `Internal Server Error`,
            message: error.message
        })
    }
}

// //Update order by id
const updateOrderById = async (req, res) => {
    const orderId = req.params.orderId;
    const { pizzaSize, pizzaType, voucher, drink, status } = req.body;
    //B2: kiểm tra dữ liệu:
    if (!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `orderId is invalid!`
        })
    }

    if (!pizzaSize) {
        return res.status(400).json({
            status: `Bad request`,
            message: `pizzaSize is required!`
        })
    }
    if (!pizzaType) {
        return res.status(400).json({
            status: `Bad request`,
            message: `pizzaType is required!`
        })
    }
    if (!voucher) {
        return res.status(400).json({
            status: `Bad request`,
            message: `voucher is required!`
        })
    }
    if (!drink) {
        return res.status(400).json({
            status: `Bad request`,
            message: `drink is required!`
        })
    }
    if (!status) {
        return res.status(400).json({
            status: `Bad request`,
            message: `status is required!`
        })
    }
    //B3: thực thi model:

    let updateOrderData = {
        pizzaSize,
        pizzaType,
        voucher,
        drink,
        status
    }
    try {
        const orderUpdated = await orderModel.findByIdAndUpdate(orderId, updateOrderData)
        if (orderUpdated) {
            return res.status(200).json({
                status: "Update order successfully",
                data: orderUpdated
            })
        }
        else {
            return res.status(404).json({
                status: `Not found any orders`,
                data: orderUpdated

            })
        }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }
}

//Delete by order by id:
const deleteOrderById = async(req, res) => {
    //B1: thu thập dữ liệu
    const orderId = req.params.orderId;
    //B2: kiểm tra dữ liệu:
    if(!mongoose.Types.ObjectId.isValid(orderId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `order id is invalid`
        })
    }
    try {
       const deletedOrder = await orderModel.findByIdAndDelete(orderId);
       if(deletedOrder) {
        return res.status(204).json({
            status: `Delete order by id successfully`,
            data: deletedOrder
        })
       }else{
        return res.status(404).json({
            status: `Not found any orders`,
        })
       }
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}
module.exports = {
    createOrder,
    getAllOrder,
    getOrderById,
    updateOrderById,
    deleteOrderById
}