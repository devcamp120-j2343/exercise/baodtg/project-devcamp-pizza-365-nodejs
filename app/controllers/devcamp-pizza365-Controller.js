//import thư viện orderModel
const { default: mongoose } = require('mongoose');
const orderModel = require('../models/orderModel');
const drinkModel = require('../models/drinkModel');
const userModel = require('../models/userModel');
const voucherModel = require('../models/voucherModel');

const createOrder = async (req, res) => {
    function makeid(length) {
        let result = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        const charactersLength = characters.length;
        let counter = 0;
        while (counter < length) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
            counter += 1;
        }
        return result;
    }
    const orderCode = makeid(8);
    //B1: thu thập dữ liệu:
    const { email, fullName, address, phone, pizzaSize, pizzaType, voucher, drink, status } = req.body;
    //B2: validate dữ liệu:
    if (!email) {
        return res.status(400).json({
            status: `Bad request`,
            message: `email is required!`
        })
    }
    if (!fullName) {
        return res.status(400).json({
            status: `Bad request`,
            message: `fullName is required!`
        })
    }
    if (!address) {
        return res.status(400).json({
            status: `Bad request`,
            message: `address is required!`
        })
    }
    if (!phone) {
        return res.status(400).json({
            status: `Bad request`,
            message: `phone is required!`
        })
    }
    if (!pizzaSize) {
        return res.status(400).json({
            status: `Bad request`,
            message: `pizzaSize is required!`
        })
    }
    if (!pizzaType) {
        return res.status(400).json({
            status: `Bad request`,
            message: `pizzaType is required!`
        })
    }
    if (!voucher) {
        return res.status(400).json({
            status: `Bad request`,
            message: `voucher is required!`
        })
    }
    if (!drink) {
        return res.status(400).json({
            status: `Bad request`,
            message: `drink is required!`
        })
    }
    if (!status) {
        return res.status(400).json({
            status: `Bad request`,
            message: `status is required!`
        })
    }
    const orderCreateData = {
        _id: new mongoose.Types.ObjectId,
        orderCode,
        email,
        pizzaSize,
        pizzaType,
        voucher,
        drink,
        status
    }
    const userCreateData = {
        _id: new mongoose.Types.ObjectId,
        email,
        fullName,
        address,
        phone
    }
    try {
        const userFoundByEmail = await userModel.findOne({ email });
        if (userFoundByEmail) {
            const userId = userFoundByEmail.id;
            const orderCreated = await orderModel.create(orderCreateData);
            if (orderCreated) {
                await userModel.findByIdAndUpdate(userId,
                    { $push: { orders: orderCreated.id } }
                )
                return res.status(200).json({
                    status: `Create order by user Id ${userId} successfully !`,
                    data: orderCreated
                })

            }
        } else {
            const newUserCreated = await userModel.create(userCreateData);
            if (newUserCreated) {
                const userId = newUserCreated.id;
                const orderCreated = await orderModel.create(orderCreateData);
                if (orderCreated) {
                    await userModel.findByIdAndUpdate(userId,
                        { $push: { orders: orderCreated.id } }
                    )
                    return res.status(200).json({
                        status: `Create order by user Id ${userId} successfully !`,
                        data: orderCreated
                    })

                }
            }


        }
    } catch (error) {
        return res.status(500).json({
            status: `Internal Server Error`,
            message: error.message
        })
    }
}

const getDrinksList = async (req, res) => {
    try {
        const drinksList = await drinkModel.find();

        if (drinksList) {
            return res.status(200).json({
                status: `Get drinks list successfully !`,
                data: drinksList
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: `Internal Server Error`,
            message: error.message
        })
    }

}

module.exports = {
    createOrder,
    getDrinksList
}