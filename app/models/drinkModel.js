//B1: Khai báo thư viên mongoose:
const mongoose = require('mongoose');

//B2: khai báo thư viện Schema của mongoose:
const Schema = mongoose.Schema;


//B3: tạo đối tượng Schema bao gồm các thuộc tính của collection trong mongoDB:
const drinkSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    maNuocUong: {
        type: String,
        unique: true,
        required: true
    },
    tenNuocUong: {
        type: String,
        required: true
    },
    donGia: {
        type: Number,
        required: true
    }, 
})

//B4: export schema ra model    
module.exports = mongoose.model('drink', drinkSchema)