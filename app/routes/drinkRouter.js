//Khai báo thư viện express
const express = require('express');
const { getAllDrinks, getDrinkById, createDrink, updateDrinkById, deleteDrink } = require('../controllers/drinkController');

//Tạo router:
const drinkRouter = express.Router();

//router get all drinks
drinkRouter.get("/drinks", getAllDrinks)

//router get drink
drinkRouter.get("/drinks/:drinkId", getDrinkById)

//router post drinks
drinkRouter.post("/drinks", createDrink)

//router put drink
drinkRouter.put("/drinks/:drinkId", updateDrinkById)

//router delete drink
drinkRouter.delete("/drinks/:drinkId", deleteDrink)

module.exports = {drinkRouter}

