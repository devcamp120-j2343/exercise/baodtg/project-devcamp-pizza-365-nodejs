//Khai báo thư viện express
const express = require('express');
const { createOrder, getDrinksList } = require('../controllers/devcamp-pizza365-Controller');

const devcampPizza365Router = express.Router();


devcampPizza365Router.post('/devcamp-pizza365/orders', createOrder)

devcampPizza365Router.get('/devcamp-pizza365/drinks', getDrinksList)


module.exports = {devcampPizza365Router}