//Khai báo thư viện express
const express = require('express');
const { createOrder, getAllOrder, getOrderById, updateOrderById, deleteOrderById } = require('../controllers/orderControllers');

//Tạo router
const orderRouter = express.Router();

//Router get all order:
orderRouter.get('/orders', getAllOrder)

//Router get order by id:
orderRouter.get('/orders/:orderId', getOrderById)

//Router post order(create new order)
orderRouter.post('/orders', createOrder)

//Router put order (update order by id)
orderRouter.put('/orders/:orderId', updateOrderById)

//Router delete order (delete order by Id):
orderRouter.delete('/orders/:orderId', deleteOrderById)

module.exports = {orderRouter}