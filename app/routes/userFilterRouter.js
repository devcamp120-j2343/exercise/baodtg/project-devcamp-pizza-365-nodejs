//Khai báo thư viện express
const express = require('express');
const { getLimitAllUsers, getSkipAllUsers, getSortAllUsers, getSkipLimitAllUsers, getSortSkipLimitAllUsers } = require('../controllers/userFilterController');

//Tạo router
const userFilterRouter = express.Router();

//Router get all user:
userFilterRouter.get('/limit-users', getLimitAllUsers)

userFilterRouter.get('/skip-users', getSkipAllUsers)

userFilterRouter.get('/sort-users', getSortAllUsers)

userFilterRouter.get('/skip-limit-users', getSkipLimitAllUsers)

userFilterRouter.get('/sort-skip-limit-users', getSortSkipLimitAllUsers)





// //Router get user by id:
// userRouter.get('/users/:userId', getUserById)

// //Router post user(create new user)
// userRouter.post('/users', createUser)

// //Router put user (update user by id)
// userRouter.put('/users/:userId', updateUserById)

// //Router delete user (delete user by Id):
// userRouter.delete('/users/:userId', deleteUserById)

module.exports = {userFilterRouter}