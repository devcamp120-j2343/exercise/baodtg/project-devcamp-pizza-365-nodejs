//Khai báo thư viện express
const express = require('express');
const { createUser, getAllUsers, getUserById, updateUserById, deleteUserById } = require('../controllers/userController');

//Tạo router
const userRouter = express.Router();

//Router get all user:
userRouter.get('/users', getAllUsers)

//Router get user by id:
userRouter.get('/users/:userId', getUserById)

//Router post user(create new user)
userRouter.post('/users', createUser)

//Router put user (update user by id)
userRouter.put('/users/:userId', updateUserById)

//Router delete user (delete user by Id):
userRouter.delete('/users/:userId', deleteUserById)

module.exports = {userRouter}