const expect = require("chai").expect;
const request = require("supertest");
const voucherModel = require("../app/models/voucherModel");
const app = require("../index");
const mongoose = require('mongoose');

describe("/vouchers", () => {
    before(async () => {
        await voucherModel.deleteMany({});
    });

    after(async () => {
        mongoose.disconnect();
    });

    it("should connect and disconnect to mongodb", async () => {
        // console.log(mongoose.connection.states);
        mongoose.disconnect();
        mongoose.connection.on('disconnected', () => {
            expect(mongoose.connection.readyState).to.equal(0);
        });
        mongoose.connection.on('connected', () => {
            expect(mongoose.connection.readyState).to.equal(1);
        });
        mongoose.connection.on('error', () => {
            expect(mongoose.connection.readyState).to.equal(99);
        });

        await mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Pizza365");
    });

    describe("GET /", () => {
        it("should return all vouchers", async () => {
            const vouchers = [
                { maVoucher: "ABC10", phanTramGiamGia: 10 },
                { maVoucher: "ABC20", phanTramGiamGia: 20 },
            ];
            await voucherModel.insertMany(vouchers);
            const res = await request(app).get("/vouchers");
            expect(res.status).to.equal(200);
            expect(res.body.data.length).to.equal(2);
        });
    });

    describe("GET /:voucherId", () => {
        it("should return a voucher if valid id is passed", async () => {
            const voucher = {
            _id:new mongoose.Types.ObjectId(),
            maVoucher: "ABC30",
            phanTramGiamGia: 30
        };
            await voucherModel.create(voucher);
            const res = await request(app).get("/vouchers/" + voucher._id);
            expect(res.status).to.equal(200);
            expect(res.body.data).to.have.property("maVoucher", voucher.maVoucher);
        });

        it("should return 400 error when invalid object id is passed", async () => {
            const res = await request(app).get("/vouchers/abc");
            expect(res.status).to.equal(400);
        });

        it("should return 404 error when valid object id is passed but does not exist", async () => {
            const res = await request(app).get("/vouchers/5f43ef20c1d4a133e4628181");
            expect(res.status).to.equal(404);
        });
    });

    describe("POST /", () => {
        it("should return voucher when the all request body is valid", async () => {
            const res = await request(app)
                .post("/vouchers")
                .send({ maVoucher: "ABC40", phanTramGiamGia: 40 });
            const data = res.body;
            expect(res.status).to.equal(201);
            expect(data.data).to.have.property("_id");
            expect(data.data).to.have.property("maVoucher", "ABC40");
            expect(data.data).to.have.property("phanTramGiamGia", 40);

            const voucher = await voucherModel.findOne({ maVoucher: 'ABC40' });
            expect(voucher.phanTramGiamGia).to.equal(40);
        });
    });

    describe("PUT /:voucherId", () => {
        it("should update the existing voucher and return 200", async () => {
            const voucher = {
                _id:new mongoose.Types.ObjectId(),
                maVoucher: "ABC45",
                phanTramGiamGia: 45
            };
            await voucherModel.create(voucher);

            const res = await request(app)
                .put("/vouchers/" + voucher._id)
                .send({ maVoucher: "ABC50",
                phanTramGiamGia: 50 });

            expect(res.status).to.equal(200);
            const dataAfterUpdate = await voucherModel.findById(voucher._id).exec();
            expect(dataAfterUpdate).to.have.property("maVoucher", "ABC50");
            expect(dataAfterUpdate).to.have.property("phanTramGiamGia", 50);
        });
    });

    let voucherId = '';
    describe("DELETE /:voucherId", () => {
        it("should delete requested id and return response 200", async () => {
            const voucher = {
                _id:new mongoose.Types.ObjectId(),
                maVoucher: "ABC60",
                phanTramGiamGia: 60
            };
            await voucherModel.create(voucher);
            voucherId = voucher._id;
            const res = await request(app).delete("/vouchers/" + voucherId);
            expect(res.status).to.be.equal(204);
        });

        it("should return 404 when deleted voucher is requested", async () => {
            let res = await request(app).get("/vouchers/" + voucherId);
            expect(res.status).to.be.equal(404);
        });
    });
});