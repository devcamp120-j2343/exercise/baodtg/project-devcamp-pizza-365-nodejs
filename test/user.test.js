const expect = require("chai").expect;
const request = require("supertest");
const userModel = require("../app/models/userModel");
const app = require("../index");
const mongoose = require('mongoose');

describe("/users", () => {
    before(async () => {
        await userModel.deleteMany({});
    });

    // after(async () => {
    //     mongoose.disconnect();
    // });

    it("should connect and disconnect to mongodb", async () => {
        // console.log(mongoose.connection.states);
        mongoose.disconnect();
        mongoose.connection.on('disconnected', () => {
            expect(mongoose.connection.readyState).to.equal(0);
        });
        mongoose.connection.on('connected', () => {
            expect(mongoose.connection.readyState).to.equal(1);
        });
        mongoose.connection.on('error', () => {
            expect(mongoose.connection.readyState).to.equal(99);
        });

        await mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Pizza365");
    });

    describe("GET /", () => {
        it("should return all users", async () => {
            const users = [
                { fullName:"Test 1",
                    email:"test1@gmail.com",
                    address:"Hanoi",
                    phone:"0359302234" },
                { fullName:"Test 2",
                    email:"test2@gmail.com",
                    address:"Hanoi",
                    phone:"0359302235" },
            ];
            await userModel.insertMany(users);
            const res = await request(app).get("/users");
            expect(res.status).to.equal(200);
            expect(res.body.data.length).to.equal(2);
        });
    });

    describe("GET /:userId", () => {
        it("should return a user if valid id is passed", async () => {
            const user = {
            _id:new mongoose.Types.ObjectId(),
            fullName:"Test 3",
            email:"test3@gmail.com",
            address:"Hanoi",
            phone:"0359302236"
        };
            await userModel.create(user);
            const res = await request(app).get("/users/" + user._id);
            expect(res.status).to.equal(200);
            expect(res.body.data).to.have.property("fullName", user.fullName);
            expect(res.body.data).to.have.property("email", user.email);
        });

        it("should return 400 error when invalid object id is passed", async () => {
            const res = await request(app).get("/users/abc");
            expect(res.status).to.equal(400);
        });

        it("should return 404 error when valid object id is passed but does not exist", async () => {
            const res = await request(app).get("/users/5f43ef20c1d4a133e4628181");
            expect(res.status).to.equal(404);
        });
    });

    describe("POST /", () => {
        it("should return user when the all request body is valid", async () => {
            const res = await request(app)
                .post("/users")
                .send({ fullName:"Test 4",
                email:"test4@gmail.com",
                address:"Hanoi",
                phone:"0359302237" });
            const data = res.body;
            expect(res.status).to.equal(201);
            expect(data.data).to.have.property("_id");
            expect(data.data).to.have.property("email", "test4@gmail.com");
            expect(data.data).to.have.property("address", "Hanoi");
            expect(data.data).to.have.property("phone", "0359302237");

            const user = await userModel.findOne({ email: "test4@gmail.com"});
            expect(user.address).to.equal('Hanoi');
            expect(user.phone).to.equal("0359302237");
        });
    });

    describe("PUT /:userId", () => {
        it("should update the existing user and return 200", async () => {
            const user = {
                _id:new mongoose.Types.ObjectId(),
                fullName:"Test 5",
                email:"test5@gmail.com",
                address:"Hanoi",
                phone:"0359302238"
            };
            await userModel.create(user);

            const res = await request(app)
                .put("/users/" + user._id)
                .send({ fullName:"Test 6",
                email:"test6@gmail.com",
                address:"Hanoi",
                phone:"0359302239" });

            expect(res.status).to.equal(200);
            const dataAfterUpdate = await userModel.findById(user._id).exec();
            expect(dataAfterUpdate).to.have.property("email", "test6@gmail.com");
            expect(dataAfterUpdate).to.have.property("phone", "0359302239");
        });
    });

    let userId = '';
    describe("DELETE /:userId", () => {
        it("should delete requested id and return response 200", async () => {
            const user = {
                _id:new mongoose.Types.ObjectId(),
                fullName:"Test 7",
                email:"test7@gmail.com",
                address:"Hanoi",
                phone:"0359302230"
            };
            await userModel.create(user);
            userId = user._id;
            const res = await request(app).delete("/users/" + userId);
            expect(res.status).to.be.equal(204);
        });

        it("should return 404 when deleted user is requested", async () => {
            let res = await request(app).get("/users/" + userId);
            expect(res.status).to.be.equal(404);
        });
    });
});