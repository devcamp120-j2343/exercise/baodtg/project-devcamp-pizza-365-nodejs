const expect = require("chai").expect;
const request = require("supertest");
const drinkModel = require("../app/models/drinkModel");
const app = require("../index");
const mongoose = require('mongoose');

describe("/", () => {
    before(async () => {
        await drinkModel.deleteMany({});
    });

    // after(async () => {
    //     mongoose.disconnect();
    // });

    it("should connect and disconnect to mongodb", async () => {
        // console.log(mongoose.connection.states);
        mongoose.disconnect();
        mongoose.connection.on('disconnected', () => {
            expect(mongoose.connection.readyState).to.equal(0);
        });
        mongoose.connection.on('connected', () => {
            expect(mongoose.connection.readyState).to.equal(1);
        });
        mongoose.connection.on('error', () => {
            expect(mongoose.connection.readyState).to.equal(99);
        });

        await mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Pizza365");
    });

    describe("GET /", () => {
        it("should return all drinks", async () => {
            const drinks = [
                { maNuocUong: "TRATAC", tenNuocUong: "trà tắc", donGia: 15000 },
                { maNuocUong: "PEPSI", tenNuocUong: "pepsi", donGia: 15000 },
            ];
            await drinkModel.insertMany(drinks);
            const res = await request(app).get('/drinks')
            console.log(drinks)

            expect(res.status).to.equal(200);
            expect(res.body.data.length).to.equal(2);
        });
    });

   
    describe("GET /:drinkId", () => {
        it("should return a drink if valid id is passed", async () => {
            const drink = {
            _id:new mongoose.Types.ObjectId(),
            maNuocUong: "COCA",
            tenNuocUong: "cocacola",
            donGia: 15000
        };
            await drinkModel.create(drink);
            const res = await request(app).get("/drinks/" + drink._id);
            expect(res.status).to.equal(200);
            expect(res.body.data).to.have.property("maNuocUong", drink.maNuocUong);
        });

        it("should return 400 error when invalid object id is passed", async () => {
            const res = await request(app).get("/drinks/abc");
            expect(res.status).to.equal(400);
        });

        it("should return 404 error when valid object id is passed but does not exist", async () => {
            const res = await request(app).get("/drinks/5f43ef20c1d4a133e4628181");
            expect(res.status).to.equal(404);
        });
    });

    describe("POST /", () => {
        it("should return drink when the all request body is valid", async () => {
            const res = await request(app)
                .post("/drinks/")
                .send({ maNuocUong: "Miranda", tenNuocUong: "miranda", donGia: 15000 });
            const data = res.body;
            expect(res.status).to.equal(201);
            expect(data.data).to.have.property("_id");
            expect(data.data).to.have.property("maNuocUong", "Miranda");
            expect(data.data).to.have.property("tenNuocUong", "miranda");
            expect(data.data).to.have.property("donGia", 15000);

            const drink = await drinkModel.findOne({ maNuocUong: 'Miranda' });
            expect(drink.tenNuocUong).to.equal('miranda');
            expect(drink.donGia).to.equal(15000);
        });
    });

    describe("PUT /:drinkId", () => {
        it("should update the existing drink and return 200", async () => {
            const drink = {
                _id:new mongoose.Types.ObjectId(),
                maNuocUong: "STRONGBOW",
                tenNuocUong: "stringbow",
                donGia: 15000
            };
            await drinkModel.create(drink);

            const res = await request(app)
                .put("/drinks/" + drink._id)
                .send({ maNuocUong: "STRONGBOW",
                tenNuocUong: "strongbow",
                donGia: 15000 });

            expect(res.status).to.equal(200);
            const dataAfterUpdate = await drinkModel.findById(drink._id).exec();
            expect(dataAfterUpdate).to.have.property("maNuocUong", "STRONGBOW");
            expect(dataAfterUpdate).to.have.property("tenNuocUong", "strongbow");
            expect(dataAfterUpdate).to.have.property("donGia", 15000);
        });
    });

    let drinkId = '';
    describe("DELETE /:drinkId", () => {
        it("should delete requested id and return response 200", async () => {
            const drink = {
                _id:new mongoose.Types.ObjectId(),
                maNuocUong: "TEAPLUs",
                tenNuocUong: "teaplus",
                donGia: 15000
            };
            await drinkModel.create(drink);
            drinkId = drink._id;
            const res = await request(app).delete("/drinks/" + drinkId);
            expect(res.status).to.be.equal(204);
        });

        it("should return 404 when deleted drink is requested", async () => {
            let res = await request(app).get("/drinks/" + drinkId);
            expect(res.status).to.be.equal(404);
        });
    });
});